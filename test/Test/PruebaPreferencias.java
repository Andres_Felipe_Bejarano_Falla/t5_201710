package Test;

import java.util.Arrays;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.logic.GeneradorDatos;
import model.logic.ManejadorPreferencias;
import model.vo.PruebaVO;
import model.vo.VOPreferencia;

public class PruebaPreferencias extends TestCase{

	ColaPrioridad<VOPreferencia> cola;
	MaxHeapCP<VOPreferencia> heap;
	ManejadorPreferencias ma = new ManejadorPreferencias();
	String s = "Drama";
	public void testPruebaColaHeapAgregar(){
		try {
			ma.cargarPeliculasSR("./data/movies.csv");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time = System.currentTimeMillis();
		heap = ma.crearMonticuloCP(s);
		System.out.println("Heap add: " + (System.currentTimeMillis()-time));
		
	}
	
	public void testPruebaColaAgregar() throws Exception{
		try {
			ma.cargarPeliculasSR("./data/movies.csv");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time = System.currentTimeMillis();
		cola= ma.crearColaP(s);
		System.out.println("Cola add: " + (System.currentTimeMillis()-time));
		
	}

	public void testPruebaColaHeapMax(){
		try {
			ma.cargarPeliculasSR("./data/movies.csv");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		heap = ma.crearMonticuloCP(s);
		long time = System.currentTimeMillis();
		for(int i =0;i<heap.darNumElementos();i++){
			heap.max();
		}
		System.out.println("Heap max: " + (System.currentTimeMillis()-time));
		
	}
	
	public void testPruebaColaMax() throws Exception{
		try {
			ma.cargarPeliculasSR("./data/movies.csv");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cola= ma.crearColaP(s);
		long time = System.currentTimeMillis();
		for(int i =0;i<cola.darNumeroElementos();i++){
			cola.max();
		}
		System.out.println("Cola max: " + (System.currentTimeMillis()-time));
		
	}
}
