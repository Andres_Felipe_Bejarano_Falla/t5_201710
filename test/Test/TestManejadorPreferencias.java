package Test;

import junit.framework.TestCase; 
import model.logic.ManejadorPreferencias;

public class TestManejadorPreferencias extends TestCase{
	ManejadorPreferencias plg;
	public void setUpEscenario1(){
		plg=new ManejadorPreferencias();
	}
	public void setUpEscenario2(){
		plg=new ManejadorPreferencias();
		try {
			plg.cargarPeliculasSR(ManejadorPreferencias.RUTA);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void testCargaDatos1(){
		setUpEscenario1();
		try {
			plg.cargarPeliculasSR(ManejadorPreferencias.RUTA);
		} catch (Exception e) {
			fail();
		}
	}
	public void testCargaDatos2(){
		setUpEscenario1();
		try {
			plg.cargarPeliculasSR("Esto no es una ruta valida(i am error)");
			fail();
		} catch (Exception e) {
			assertEquals("La ruta no es v�lida.", e.getMessage());
		}
	}
}
