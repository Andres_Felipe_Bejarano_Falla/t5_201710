package Test;

import junit.framework.TestCase; 
import model.data_structures.ColaPrioridad;
import model.logic.GeneradorDatos;
import model.vo.PruebaVO;

public class TestColaPrioridad extends TestCase {
	ColaPrioridad<PruebaVO> cola = new ColaPrioridad<>();
	GeneradorDatos k = new GeneradorDatos();

	public void setUpEscenario1() {
		cola.crearCP(0);
	}

	public void setUpEscenario2() {
		cola.crearCP(10);
		int[] � = k.generarAgnos(10);
		String[] q = k.generarCadenas(10);
		for (int i = 0; i < 10; i++) {
			PruebaVO y = new PruebaVO(q[i], �[i]);
			try {
				cola.agregar(y);
			} catch (Exception e) {
				//
			}
		}
	}

	public void setUpEscenario3() {
		cola.crearCP(10);
	}

	public void testAgregar1() {
		setUpEscenario1();
		try {
			cola.agregar(new PruebaVO(k.generarCadenas(1)[0], k.generarAgnos(1)[0]));
		} catch (Exception e) {
			assertEquals("Cola de prioridad llena", e.getMessage());
		}
	}

	public void testAgregar2() {
		setUpEscenario2();
		try {
			cola.agregar(new PruebaVO(k.generarCadenas(1)[0], k.generarAgnos(1)[0]));
		} catch (Exception e) {
			assertEquals("Cola de prioridad llena", e.getMessage());
		}
	}

	public void testAgregar3() {
		setUpEscenario3();
		try {
			cola.agregar(new PruebaVO(k.generarCadenas(1)[0], k.generarAgnos(1)[0]));
		} catch (Exception e) {
			fail();
		}
	}

	public void testAgregar4() {
		setUpEscenario3();
		try {
			cola.agregar(new PruebaVO("El Padrino", 1972));
			cola.agregar(new PruebaVO("Dawn Of The Dead", 2004));
			cola.agregar(new PruebaVO("Taxi Driver", 1976));
			cola.agregar(new PruebaVO("Fight Club", 1999));
		} catch (Exception e) {
			fail();
		}
		assertEquals(0, cola.max().compareTo(new PruebaVO("Dawn Of The Dead", 2004)));
		assertEquals(0, cola.max().compareTo(new PruebaVO("Fight Club", 1999)));
		assertEquals(0, cola.max().compareTo(new PruebaVO("Taxi Driver", 1976)));
		assertEquals(0, cola.max().compareTo(new PruebaVO("El Padrino", 1972)));
	}

	public void testEsVacio1() {
		setUpEscenario1();
		assertTrue(cola.esVacia());
	}

	public void testEsVacio2() {
		setUpEscenario2();
		assertFalse(cola.esVacia());
	}

	public void testEsVacio3() {
		setUpEscenario3();
		assertTrue(cola.esVacia());
	}

	public void testTamanoMax() {

		// 1
		setUpEscenario1();
		assertEquals(0, cola.tamanoMax());

		// 2
		setUpEscenario2();
		assertEquals(10, cola.tamanoMax());

		// 3
		setUpEscenario3();
		assertEquals(10, cola.tamanoMax());
	}

	public void testDarNumElementos(){

		// 1
		setUpEscenario1();
		assertEquals(0, cola.darNumeroElementos());

		// 2
		setUpEscenario2();
		assertEquals(10, cola.darNumeroElementos());

		// 3
		setUpEscenario3();
		assertEquals(0, cola.darNumeroElementos());
	}

	public void testMax1(){

		// 1
		setUpEscenario1();
		assertNull(cola.max());

		// 2
		setUpEscenario2();
		assertNotNull(cola.max());

		// 3
		setUpEscenario3();
		assertNull(cola.max());
	}

	public void testMax2(){
		setUpEscenario3();
		try {
			cola.agregar(new PruebaVO("Pulp Fiction", 1994));
		} catch (Exception e) {
			fail();
		}
		assertEquals(0, cola.max().compareTo(new PruebaVO("Pulp Fiction", 1994)));
	}
}