package Test;

import java.util.Arrays;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.logic.GeneradorDatos;
import model.vo.PruebaVO;

public class PruebaColaPrioridad extends TestCase{

	ColaPrioridad<PruebaVO> cola;
	MaxHeapCP<PruebaVO> heap;
	GeneradorDatos generador = new GeneradorDatos();
	int n =24000;
	
	public void testPruebaColaHeapAgregar(){
		heap= new MaxHeapCP<>();
		heap.crearCP(n);
		int[] a = generador.generarAgnos(n);
		String[] b = generador.generarCadenas(n);
		PruebaVO[] pru = new PruebaVO[n];
		for(int i =0;i<n;i++){
			PruebaVO p = new PruebaVO(b[i], a[i]);
			pru[i] = p;
		}
		long time = System.currentTimeMillis();
		for(int i =0;i<n;i++){
			heap.agregar(pru[i]);
		}
		System.out.println("Heap add: " + (System.currentTimeMillis()-time));
		
		//Se ordena el areglo base con el sort propio de java
		Arrays.sort(pru);
		
		for(int i =0;i<n;i++){
			//Se comparan el max del heap con el ultimo del arreglo ya que el .sort lo ordena de menor a mayor.
			assertEquals(pru[pru.length-1-i], heap.max());
		}
	}
	
	public void testPruebaColaAgregar() throws Exception{
		cola= new ColaPrioridad<>();
		cola.crearCP(n);
		int[] a = generador.generarAgnos(n);
		String[] b = generador.generarCadenas(n);
		PruebaVO[] pru = new PruebaVO[n];
		for(int i =0;i<n;i++){
			PruebaVO p = new PruebaVO(b[i], a[i]);
			pru[i] = p;
		}
		long time = System.currentTimeMillis();
		for(int i =0;i<n;i++){
			cola.agregar(pru[i]);
		}
		System.out.println("Cola add: " + (System.currentTimeMillis()-time));
		
		//Se ordena el areglo base con el sort propio de java
		Arrays.sort(pru);
		
		for(int i =0;i<n;i++){
			//Se comparan el max del heap con el ultimo del arreglo ya que el .sort lo ordena de menor a mayor.
			assertEquals(pru[pru.length-1-i], cola.max());
		}
	}

	public void testPruebaColaHeapMax(){
		heap= new MaxHeapCP<>();
		heap.crearCP(n);
		int[] a = generador.generarAgnos(n);
		String[] b = generador.generarCadenas(n);
		PruebaVO[] pru = new PruebaVO[n];
		for(int i =0;i<n;i++){
			PruebaVO p = new PruebaVO(b[i], a[i]);
			pru[i] = p;
		}
		for(int i =0;i<n;i++){
			heap.agregar(pru[i]);
		}
		long time = System.currentTimeMillis();
		for(int i =0;i<n;i++){
			heap.max();
		}
		System.out.println("Heap max: " + (System.currentTimeMillis()-time));

	}
	
	public void testPruebaColaMax() throws Exception{
		cola= new ColaPrioridad<>();
		cola.crearCP(n);
		int[] a = generador.generarAgnos(n);
		String[] b = generador.generarCadenas(n);
		PruebaVO[] pru = new PruebaVO[n];
		for(int i =0;i<n;i++){
			PruebaVO p = new PruebaVO(b[i], a[i]);
			pru[i] = p;
		}
		for(int i =0;i<n;i++){
			cola.agregar(pru[i]);
		}
		long time = System.currentTimeMillis();
		for(int i =0;i<n;i++){
			cola.max();
		}
		System.out.println("Cola max " + (System.currentTimeMillis()-time));
		
		//Se ordena el areglo base con el sort propio de java
		Arrays.sort(pru);
		
	}
}
