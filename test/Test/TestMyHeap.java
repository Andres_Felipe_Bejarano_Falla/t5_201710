package Test;

import java.util.Arrays;

import junit.framework.TestCase;
import model.data_structures.MaxHeapCP;
import model.logic.GeneradorDatos;
import model.logic.ManejadorPreferencias;
import model.vo.PruebaVO;
import model.vo.VOPreferencia;

public class TestMyHeap extends TestCase {
	
	MaxHeapCP<PruebaVO> heap;
	GeneradorDatos generador = new GeneradorDatos();
	
	public void testEscenario1(){
		heap= new MaxHeapCP<>();
		heap.crearCP(0);
		assertNull(heap.max());
		assertTrue(heap.esVacia());
		assertEquals(0, heap.darNumElementos());
	}
	
	public void testEscenario2(){
		heap= new MaxHeapCP<>();
		heap.crearCP(10);
		heap.agregar(new PruebaVO("Holi", 2000));
		assertFalse(heap.esVacia());
		assertEquals(1, heap.darNumElementos());
		PruebaVO p = heap.max();
		assertEquals("Holi", p.getNombre());
		assertEquals(2000, p.getAgno());
		assertTrue(heap.esVacia());
		assertEquals(0, heap.darNumElementos());
	}
	
	public void testCargaDatos3(){
		heap= new MaxHeapCP<>();
		heap.crearCP(10);
		heap.agregar(new PruebaVO("A", 2005));
		heap.agregar(new PruebaVO("A", 2000));
		heap.agregar(new PruebaVO("B", 2000));
		heap.agregar(new PruebaVO("C", 2000));
		heap.agregar(new PruebaVO("D", 2000));
		heap.agregar(new PruebaVO("E", 2000));
		heap.agregar(new PruebaVO("F", 2000));
		heap.agregar(new PruebaVO("H", 2001));
		heap.agregar(new PruebaVO("H", 2002));
		assertFalse(heap.esVacia());
		assertEquals(9, heap.darNumElementos());
		PruebaVO p = heap.max();
		assertEquals("A", p.getNombre());
		assertEquals(2005, p.getAgno());
		p = heap.max();
		assertEquals("H", p.getNombre());
		assertEquals(2002, p.getAgno());
		p = heap.max();
		assertEquals("H", p.getNombre());
		assertEquals(2001, p.getAgno());
		p = heap.max();
		assertEquals("F", p.getNombre());
		assertEquals(2000, p.getAgno());
		p = heap.max();
		assertEquals("E", p.getNombre());
		assertEquals(2000, p.getAgno());
		p = heap.max();
		assertEquals("D", p.getNombre());
		assertEquals(2000, p.getAgno());
		p = heap.max();
		assertEquals("C", p.getNombre());
		assertEquals(2000, p.getAgno());
		assertFalse(heap.esVacia());
		assertEquals(2, heap.darNumElementos());
	}
	
	public void testCargaDatos4(){
		heap= new MaxHeapCP<>();
		
		heap.crearCP(5);
		heap.agregar(new PruebaVO("A", 2005));
		heap.agregar(new PruebaVO("A", 2000));
		heap.agregar(new PruebaVO("B", 2000));
		heap.agregar(new PruebaVO("C", 2000));
		heap.agregar(new PruebaVO("D", 2000));
		//Se agregan unicamente los 5 primeros
		heap.agregar(new PruebaVO("E", 2000));
		heap.agregar(new PruebaVO("F", 2000));
		heap.agregar(new PruebaVO("H", 2001));
		heap.agregar(new PruebaVO("H", 2002));
		assertEquals(5, heap.darNumElementos());
		PruebaVO p = heap.max();
		assertEquals("A", p.getNombre());
		assertEquals(2005, p.getAgno());
		p = heap.max();
		assertEquals("D", p.getNombre());
		assertEquals(2000, p.getAgno());
		p = heap.max();
		assertEquals("C", p.getNombre());
		assertEquals(2000, p.getAgno());
	}
	
	public void testCargaDatos5(){
		heap= new MaxHeapCP<>();
		heap.crearCP(500);
		int[] a = generador.generarAgnos(500);
		String[] b = generador.generarCadenas(500);
		PruebaVO[] pru = new PruebaVO[500];
		for(int i =0;i<500;i++){
			PruebaVO p = new PruebaVO(b[i], a[i]);
			pru[i] = p;
		}
		for(int i =0;i<500;i++){
			heap.agregar(pru[i]);
		}
		
		//Se ordena el areglo base con el sort propio de java
		Arrays.sort(pru);
		
		for(int i =0;i<500;i++){
			//Se comparan el max del heap con el ultimo del arreglo ya que el .sort lo ordena de menor a mayor.
			assertEquals(pru[pru.length-1-i], heap.max());
		}
	}
	
}
