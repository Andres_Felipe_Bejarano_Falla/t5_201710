package model.logic;

import java.util.concurrent.ThreadLocalRandom;

public class GeneradorDatos {

	private final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-";

	public String[] generarCadenas(int N) {
		int count;
		String[] array = new String[N];

		for (int i = 0; i < array.length; i++) {
			StringBuilder builder = new StringBuilder();
			count = 25;
			while (count-- != 0) {
				int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
				builder.append(ALPHA_NUMERIC_STRING.charAt(character));
			}
			array[i] = builder.toString();
		}
		return array;
	}

	public int[] generarAgnos(int N) {
		int[] pensar = new int[N];
		for (int j = 0; j < pensar.length; j++) {
			pensar[j] = ThreadLocalRandom.current().nextInt(1950, 2018);
		}
		return pensar;
	}
	

}
