package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import model.data_structures.ColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.vo.PruebaVO;
import model.vo.VOPreferencia;

public class ManejadorPreferencias {
	
	public static final String RUTA="./data/movies.csv";
	
	ArrayList<VOPreferencia> k = new ArrayList<>();

	public boolean cargarPeliculasSR(String rutaPeliculas) throws Exception {
		boolean res = false;
		BufferedReader buff = null;
		String sepCvs = ",";
		String line = "";
		int cont = 0;
		try {
			buff = new BufferedReader(new FileReader(rutaPeliculas));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				String generos = datos[datos.length - 1];
				String nombre = "";
				for (int i = 1; i < datos.length - 1; i++) {
					nombre = nombre + datos[i];
				}

				if (nombre.startsWith("\"") && nombre.endsWith("\"")) {
					nombre = nombre.substring(1, nombre.length() - 1);
				}

				int a�o = -1;

				if (nombre.endsWith(")")) {
					Character g = nombre.charAt(nombre.length() - 2);
					if (g.isDigit(g)) {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 5, nombre.length() - 1));
					} else {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 6, nombre.length() - 2));
					}
				}

				VOPreferencia k = new VOPreferencia(nombre, a�o);
				k.agregarGeneros(generos);
				this.k.add(k);
			}
			res = true;
			buff.close();
		} catch (Exception e) {
			throw new Exception("La ruta no es v�lida.");
		}
		return res;
	}

	public MaxHeapCP<VOPreferencia> crearMonticuloCP(String genero) {
		ArrayList<VOPreferencia> porGenero = new ArrayList<>();
		for (int i = 0; i < k.size(); i++)
			if (k.get(i).compararGenero(genero))
				porGenero.add(k.get(i));
		MaxHeapCP<VOPreferencia> heap = new MaxHeapCP<>();
		heap.crearCP(porGenero.size());
		for (VOPreferencia p : porGenero) {
			try {
				heap.agregar(p);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return heap;
	}
	
	public ColaPrioridad<VOPreferencia> crearColaP(String genero) {
		ArrayList<VOPreferencia> porGenero = new ArrayList<>();
		for (int i = 0; i < k.size(); i++)
			if (k.get(i).compararGenero(genero))
				porGenero.add(k.get(i));
		ColaPrioridad<VOPreferencia> jpk = new ColaPrioridad<>();
		jpk.crearCP(porGenero.size());
		for (VOPreferencia p : porGenero) {
			try {
				jpk.agregar(p);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return jpk;
	}

	// public static void main(String[] args) {
	// ManejadorPreferencias p = new ManejadorPreferencias();
	// p.cargarPeliculasSR("./data/movies.csv");
	// ColaPrioridad<PruebaVO> plk=p.crearColaP("comedy");
	// for (int i = 0; i < plk.tamanoMax(); i++) {
	// System.out.println(plk.max());
	// }
	// }
}
