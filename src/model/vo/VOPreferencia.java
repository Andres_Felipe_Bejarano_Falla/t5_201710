package model.vo;

public class VOPreferencia implements Comparable<VOPreferencia>{

	private String nombre;
	private int agno;
	private String generos[];
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAgno() {
		return agno;
	}

	public void setAgno(int agno) {
		this.agno = agno;
	}


	public VOPreferencia(String gh, int anio) {
		nombre = gh.substring(0, gh.length() - 7);
		agno = anio;
	}

	public int compareTo(VOPreferencia o) {
		if (agno - o.agno < 0)
			return -1;
		if (agno - o.agno > 0)
			return 1;
		return nombre.compareTo(o.nombre);
	}

	public void agregarGeneros(String s) {
		String genero = s.replace("|", "_");
		generos = genero.split("_");
	}

	public boolean compararGenero(String pGenero) {
		for (int i = 0; i < generos.length; i++)
			if (pGenero.equalsIgnoreCase(generos[i]))
				return true;
		return false;
	}

	public String toString() {
		String holi = "";
		for (int i = 0; i < generos.length; i++) {
			holi += "-" + generos[i];
		}
		return "( " + nombre + ", " + agno + " [" + holi.substring(1) + "] )";
	}
}
