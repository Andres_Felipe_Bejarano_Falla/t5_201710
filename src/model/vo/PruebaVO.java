package model.vo;

public class PruebaVO implements Comparable<PruebaVO> {

	private String nombre;
	private int agno;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAgno() {
		return agno;
	}

	public void setAgno(int agno) {
		this.agno = agno;
	}


	public PruebaVO(String gh, int anio) {
		nombre = gh;
		agno = anio;
	}

	public int compareTo(PruebaVO o) {
		if (agno - o.agno < 0){
			return -1;	
		}
		if (agno - o.agno > 0){
			return 1;
		}
		return nombre.compareTo(o.nombre);
	}

	public String toString() {
		return "( " + nombre + ", " + agno + " )";
	}
}