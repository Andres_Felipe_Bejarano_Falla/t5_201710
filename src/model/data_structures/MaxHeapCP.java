package model.data_structures;

public class MaxHeapCP <Key extends Comparable<Key>> {
	
	private Key[] heap;
	
	private int N = 0;
	
	private int tamanoMax;
	
	public void crearCP(int MaxN){
		tamanoMax = MaxN;
		heap = (Key[])new Comparable[MaxN+1];
	}
	
	public boolean esVacia(){
		return N ==0;
	}
	public int darNumElementos(){
		return N;
	}
	
	public int tamanaMax(){
		return tamanoMax;
	}
	
	private boolean less(int i,int j){
		return heap[i].compareTo(heap[j]) < 0;
	}
	
	private void exch(int i , int j){
		Key t = heap[i];
		heap[i] = heap[j];
		heap[j] = t;
	}
	
	private void swim(int k){
		while(k > 1&& less( k/2,k)){
			exch(k/2, k); 
			k = k/2;
		}
	}
	
	private void sink(int k){
		while(k*2 <= N){
			int j = k*2;
			if(j<N && less(j,j+1))j++;
			if(!less(k,j))break;
			exch(k, j);
			k = j;
		}
	}
	
	public void agregar(Key e){
		if(N+1< heap.length){
		N++;
		heap[N] = e;
		swim(N);
		}
	}
	
	public Key max(){
		if(N<1)return null;
		Key r = heap[1];
		exch(1, N);
		heap[N--] = null;
		sink(1);
		return r;
	}
	
}
